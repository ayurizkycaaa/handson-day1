import java.util.Scanner;

public class CharacterizedString{
    public static void main(String[] args){
        String string_penuh;
        System.out.print("Ketik kata/kalimat, lalu tekan enter : ");
        try (Scanner keyboard = new Scanner(System.in)) {
            string_penuh = keyboard.nextLine();
        }
        CharacterizedString.Line();
        System.out.println("String Penuh : " + string_penuh);
        CharacterizedString.Line();
        System.out.println("Proses memecahkan kata/kalimat menjadi karakter.....");
        System.out.println("Done");
        CharacterizedString.Line();

        for(int i=0; i<string_penuh.length();i++){  
            char karakter = string_penuh.charAt(i);  
            System.out.println("Karakter [" + (i+1) + "] : "+ karakter);  
        }   
    }

    public static void Line(){
        System.out.println("======================================");
    }
}